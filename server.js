var express = require('express'),
 app = express(),
 port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res){
res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/Clientes/:idCliente', function (req,res){
 res.send("Aqui tiene el cliente numero"+ req.params.idCliente);

});

app.post('/', function (req,res){
 res.send("Hemos recibido su peticion POST");
});

app.put('/', function (req,res){
 res.send("Hemos recibido su peticion put cambiada");
});

app.delete('/', function (req,res){
 res.send("Hemos recibido su peticion DELETE");
});
//var movimientosJSON

var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function(req, res){
//  res.sendFile(path.join(__dirname, 'movimientosv1.json'));
  res.send(movimientosJSON)
});
